const roles = [
	{role:"driver",name:"Водитель"},
	{role:"cook",name:"Повар"},
	{role:"waiter",name:"Официант"},
];

export default function (state=roles,action){
	switch(action.type){
		default:
			return state;
	}
}
