import listEmployees from "../assets/employees.json";
import moment from "moment";

let employees = [...listEmployees];
let empId = 17;

function sortEmployee(type,newEmployees){
	if(type==="fio"){
		return newEmployees.sort(function (a, b) {
			if (a.name > b.name) {
				return 1;
			}
			if (a.name < b.name) {
				return -1;
			}
			return 0;
		});
	} else if(type==="date") {
		return newEmployees.sort(function (a, b) {
			if (moment(a.birthday, "DD.MM.YYYY").isAfter(moment(b.birthday, "DD.MM.YYYY"))) {
				return 1;
			}
			if (moment(a.birthday, "DD.MM.YYYY").isBefore(moment(b.birthday, "DD.MM.YYYY"))) {
				return -1;
			}
			return 0;
		});
	}
	return newEmployees;
}
function filterEmployee(filter){
	
	const {isArchive,role} = filter;
	
	let newEmployees = [...employees];

	if(isArchive===true){
		newEmployees = newEmployees.filter(emp=>emp.isArchive);	
	}
	if(role){
		newEmployees = newEmployees.filter(emp=>emp.role===role);
	}
	return newEmployees;
}
function updateEmployee(employee){
	employees = employees.map(emp=>{
		if(emp.id===employee.id){
			return {...emp,...employee};
		}
		return emp;
	});
	return employees;
}
function createEmployee(employee){
	empId+=1;
	employees = [...employees,{...employee,id:empId}];
	return [...employees];
}

export default function (state=employees,action){
	switch(action.type){
		case "ACTIVE_EMPLOYEES":
			return employees;
		case "FILTER_EMPLOYEES":
			return filterEmployee(action.payload.filter);
		case "SORT_EMPLOYEES":
			return sortEmployee(action.payload.type,[...state]);;
		case "CREATE_EMPLOYEE":
			return createEmployee(action.payload.employee);
		case "UPDATE_EMPLOYEE":
			return updateEmployee(action.payload.employee);
		default:
			return state;
	}
}


