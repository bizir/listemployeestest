import {combineReducers} from 'redux';

import EmployeesActive from './employees-active';
import RolesActive from './roles-active';


const allReducers = combineReducers ({
	employees:EmployeesActive,
	roles:RolesActive,
});

export default allReducers;
