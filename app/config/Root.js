import 'babel-polyfill';
import React from 'react';

import WebPage from '../routes/WebPage'
import Employee from '../routes/Employee'

import allReducers from '../reducers';
import { BrowserRouter,Switch,Route } from 'react-router-dom';

import {Provider} from 'react-redux';

import {createStore , applyMiddleware} from "redux";

import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';

const store = createStore(allReducers,composeWithDevTools(applyMiddleware(thunk)));
const App = () => (
	<BrowserRouter>
		<Provider store={store}>
			<Switch>
				<Route path="/" component={WebPage} exact />
				<Route exact path='/employee/:employeeId' component={Employee}/>
			</Switch>
		</Provider>
	</BrowserRouter>
);
export default App;