import React from 'react';
import { Input } from 'antd';
import {TextMask} from 'react-text-mask-hoc';

function InputMask({value,mask,onChange}){
	return(
		<TextMask
			Component={Input}
			mask={mask}
			value={value}
			guide={false}
			onChange={(e,e1)=>{
				onChange(e1.value)
			}}
			showMask
		/>
	)
}
export default InputMask;