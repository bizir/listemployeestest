import React from 'react';
import {Form,Button,Input,Select,Checkbox,Row} from 'antd';

import PropTypes from 'prop-types';
import {updateEmployee,createEmployee} from "../actions/index";

import InputMask from '../components/InputMask';

import {bindActionCreators} from  'redux';
import {connect} from 'react-redux';

import "./Employee.less";

const FormItem = Form.Item;
const {Option} = Select;

function Employee({employees,match,roles,form,updateEmployee,createEmployee,history}){
	const {getFieldDecorator} = form;
	const {employeeId} = match.params;
	
	let employee = employees.find(emp=>emp.id===parseInt(employeeId,10));
	let content = <p className="not_find">Нет такого сотрудника</p>;
	
	let title = <h2>Редактировать сотрудника</h2>;
		
	if(employeeId==="create"){
		employee={};
		title = <h2>Добавить сотрудника</h2>;
	}
	
	if(employee){
		content = 	<Form onSubmit={(e)=>{
						e.preventDefault();
						form.validateFields((err, values) => {
							if (!err) {
								if(employeeId==="create"){
									createEmployee(values);
								} else {
									updateEmployee({...employee,...values});
								}
								history.push("/");
							}
						});
						
					}}>
						<FormItem label="Название">
						{
							getFieldDecorator('name', {
								initialValue:employee.name,
								rules: [
									{ required: true, message: 'Введите имя сотрудника' },
									
								],
							})(
								<Input/>
							)
						}
						</FormItem>
						<FormItem label="Телефон">
						{
							getFieldDecorator('phone', {
								initialValue:employee.phone,
								rules: [
									{ required: true, message: 'Введите номер телефона' },
									{ pattern: /\+7 \([1-9]\d{2}\) \d{3}-\d{4}/, message: 'Введите правильный номер телефона' },
									
								],
							})(
								<InputMask mask={['+','7',' ','(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}/>
							)
						}
						</FormItem>
						<FormItem label="День рождения">
						{
							getFieldDecorator('birthday', {
								initialValue:employee.birthday,
								rules: [
									{ required: true, message: 'Введите дату' },
									{ pattern: /[0-3][0-9]\.[0-1][0-9]\.[0-2]\d{3}/, message: 'Введите корректную дату' },
								],
							})(
								<InputMask mask={[/\d/,/\d/,'.',/\d/,/\d/,'.',/\d/,/\d/,/\d/,/\d/]}/>
							)
						}
						</FormItem>
						<FormItem label="Должность">
						{
							getFieldDecorator('role', {
								initialValue:employee.role,
								rules: [
									{ required: true, message: 'Выберите должность' }
								],
							})(
								<Select>
								{
									roles.map(role=>
										<Option key={role.role} value={role.role}>{role.name}</Option>
									)
								}
								</Select>
							)
						}
						</FormItem>
						<FormItem label="Добавить в архив">
						{
							getFieldDecorator('isArchive', {
								initialValue:employee.isArchive===true,
								valuePropName: 'checked',
							})(
								<Checkbox>Добавить в архив</Checkbox>
							)
						}
						</FormItem>
						
						<Button type="primary" htmlType="submit">{(employeeId==="create")?"Создать":"Изменить"}</Button>
					</Form>
	}
	return(
		<Row className="employee">
			{title}
			{content}
		</Row>
	)
}
Employee.propTypes = {
	employees: 	PropTypes.arrayOf(
					PropTypes.shape({
						id: PropTypes.number.isRequired,
						name: PropTypes.string.isRequired,
						isArchive: PropTypes.bool.isRequired,
						role: PropTypes.string.isRequired,
						phone: PropTypes.string.isRequired,
						birthday: PropTypes.string.isRequired,
					})
				).isRequired,
	roles:	PropTypes.arrayOf(
					PropTypes.shape({
						role: PropTypes.string.isRequired,
						name: PropTypes.string.isRequired,
					})
				).isRequired,
	match:	PropTypes.shape({
				params: PropTypes.shape({
					employeeId: PropTypes.string.isRequired,
				}).isRequired,
			}).isRequired,
};
function mapStateToProps (state) {
	return {
		employees:state.employees,
		roles:state.roles,
	};
}
function matchDispatchToProps(dispatch){
	return bindActionCreators({updateEmployee,createEmployee},dispatch);
}
export default Form.create()(connect(mapStateToProps,matchDispatchToProps)(Employee));