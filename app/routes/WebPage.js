import React from 'react';
import { List,Row,Col,Radio,Form,Select,Checkbox,Button} from 'antd';

import PropTypes from 'prop-types';

import {sortEmployees,filterEmployees} from "../actions/index";

import {bindActionCreators} from  'redux';
import {connect} from 'react-redux';

import "./WebPage.less";

const FormItem = Form.Item;
const {Option} = Select;

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

function WebPage({employees,roles,sortEmployees,filterEmployees,form,history}){
	const {getFieldDecorator} = form;
	
	return(
		<Row className="main-page">
			<h2 className="heading">Список сотрудников</h2>
			<Col xs={24} sm={9} md={6} lg={6} className={"filter"} >
				<Row>
					<RadioGroup onChange={(e)=>{sortEmployees(e.target.value)}} defaultValue="a">
						<RadioButton value="fio">По фамилии</RadioButton>
						<RadioButton value="date">По дате</RadioButton>
					</RadioGroup>
				</Row>
				<Form onSubmit={(e)=>{
					e.preventDefault();
					form.validateFields((err, values) => {
						if (!err) {
							filterEmployees(values);
						}
					});
					
				}}>
					<FormItem label="Должность">
					{
						getFieldDecorator('role', {
							valuePropName: 'value',
						})(
							<Select
								allowClear
							>
							{
								roles.map(role=>
									<Option 
										key={role.role} 
										value={role.role}
									>
										{role.name}
									</Option>
								)
							}
							</Select>
						)
					}
					</FormItem>
					<FormItem label="">
					{
						getFieldDecorator('isArchive', {
							valuePropName: 'checked',
						})(
							<Checkbox>
								В архиве
							</Checkbox>
						)
					}
					</FormItem>
					<Col className="buttonConteiner" xs={24} sm={12} md={12} lg={12} >
						<Button type="primary" htmlType="submit">Найти</Button>
					</Col>
					<Col className="buttonConteiner" xs={24} sm={12} md={12} lg={12} >
						<Button 
							type="primary" 
							onClick={()=>{history.push("/employee/create")}} 
							htmlType="submit"
						>
							Создать
						</Button>
					</Col>
					
				</Form>
			</Col>
			<Col xs={24} sm={15} md={18} lg={18} >
				<List
					itemLayout="horizontal"
					dataSource={employees}
					renderItem={item => (
						<List.Item 
							key={item.id}
							onClick={()=>{
								history.push("/employee/"+item.id);
							}
						}>
								<List.Item.Meta
									title={item.name}
									description={
										<Row>
											<p>{item.phone}</p>
											<p>{roles.find(role=>item.role===role.role).name}</p>
										</Row>
									}
								/>
						</List.Item>
					)}
				/>
			</Col>
		</Row>
	)
}
WebPage.propTypes = {
	employees: 	PropTypes.arrayOf(
					PropTypes.shape({
						id: PropTypes.number.isRequired,
						name: PropTypes.string.isRequired,
						isArchive: PropTypes.bool.isRequired,
						role: PropTypes.string.isRequired,
						phone: PropTypes.string.isRequired,
						birthday: PropTypes.string.isRequired,
					})
				).isRequired,
	roles:	PropTypes.arrayOf(
					PropTypes.shape({
						role: PropTypes.string.isRequired,
						name: PropTypes.string.isRequired,
					})
				).isRequired 
};
function mapStateToProps (state) {
	return {
		employees:state.employees,
		roles:state.roles,
	};
}
function matchDispatchToProps(dispatch){
	return bindActionCreators(
	{
		sortEmployees,
		filterEmployees
	},dispatch);
}
export default Form.create()(connect(mapStateToProps,matchDispatchToProps)(WebPage));