import { message } from 'antd';
export const sortEmployees = (type) => dispatch=> {
	dispatch({type:'SORT_EMPLOYEES',payload:{type}});
};
export const filterEmployees = (filter) => dispatch=> {
	dispatch({type:'FILTER_EMPLOYEES',payload:{filter}});
};
export const updateEmployee = (employee) => dispatch=> {
	dispatch({type:'UPDATE_EMPLOYEE',payload:{employee}});
	message.success('Пользователь обновлен');
};
export const createEmployee = (employee) => dispatch=> {
	dispatch({type:'CREATE_EMPLOYEE',payload:{employee}});
	message.success('Пользователь добавлен');
};